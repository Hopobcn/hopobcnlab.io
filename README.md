# Hopobcn's personal webpage [![pipeline status](https://gitlab.com/Hopobcn/hopobcn.gitlab.io/badges/develop/pipeline.svg)](https://gitlab.com/Hopobcn/hopobcn.gitlab.io/commits/develop)

Personal webpage to improve my skills in web development & static websites
that might also be useful to document my personal projects and to acumulate all interesting info.

## Contact
If you wish to contact me regarding this theme please raise an issue on GitHub,
tweet me [@_jacobtomlinson](http://www.twitter.com/_jacobtomlinson) or email me
[jacob@jacobtomlinson.co.uk](mailto:jacob@jacobtomlinson.co.uk).

## Jekyll Theme: [carte-noire](https://github.com/jacobtomlinon/carte-noire)
Carte-noire is a Jekyll theme develop by Jacob Tomlinson.

## License
The jekyll theme, HTML, CSS and JavaScript is licensed under GPLv3 (unless stated otherwise in the file).
