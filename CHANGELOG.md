# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Removed

## [0.1.0] - 2017-11-11
### Added
- Initial version with Carte Noire Them
### Changed
### Removed

[Unreleased]: https://gitlab.com/Hopobcn/hopobcn.gitlab.io/compare/v0.1.0...HEAD
[0.1.1]: https://gitlab.com/Hopobcn/hopobcn.gitlab.io/compare/v0.1.0...v0.1.1
